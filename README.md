# Confetti Canon

Code for the Raspberry Pi to fire the AI Labs confetti Canon. Long polls a SQS queue in AWS for new messages

## Setup

### Setup the virtual environment

```bash
export https_proxy=httpproxy.bfm.com:8080
export no_proxy=developer.blackrock.com,git.blackrock.com
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt --trusted-host developer.blackrock.com
```

### Authenicate using blk-aws-cli

```bash
blk-aws auth
```

## Running

### Starting the listener

```bash
python confetticanon/listener.py
```

### Pushing a message onto the queue

```bash
python confetticanon/push.py
```
