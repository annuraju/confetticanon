from gpiozero import LED


def switch_on_status(status: str):
    if status == "Passed":
        print("Switching ON GREEN")
        led = LED(26)
        led.on()
    elif status == "Failed":
        print("Switching ON RED")
        led = LED(21)
        led.on()
    else:
        print("Not a valid status")


def led_on(number: int):
    led = LED(number)
    led.on()


def led_off(number: int):
    led = LED(number)
    led.off()


def led_toggle(number: int):
    led = LED(number)
    led.toggle()