import json

import boto3

from confetticanon import config
from confetticanon.gpio import switch_on_status
import logging
from gpiozero import LED
logger = logging.getLogger(__name__)
green = LED(26)
red = LED(21)

class SQSListener:

    def listen(self):
        sqs = boto3.client('sqs', region_name="us-east-1")
        while True:
            try:
                response = sqs.receive_message(
                    QueueUrl=config.QUEUE_URL,
                    AttributeNames=[
                        'SentTimestamp'
                    ],
                    MaxNumberOfMessages=1,
                    MessageAttributeNames=[
                        'All'
                    ],
                    WaitTimeSeconds=3
                )
                if "Messages" in response.keys():
                    for m in response['Messages']:
                        body = json.loads(m['Body'])
                        print("Event {0}".format(str(body)))
                        print("Event Status: {0}".format(str(body["eventStatus"])))
                        status = body['eventStatus']
                        if status == "Passed":
                            red.off()
                            print("Switching ON GREEN")
                            green.blink()
                            print("Green Done")
                        elif status == "Failed":
                            green.off()
                            print("Switching ON RED")
                            red.blink()
                            print("Red Done")
                        else:
                            print("Not a valid status")
                        sqs.delete_message(
                            QueueUrl=config.QUEUE_URL,
                            ReceiptHandle=m['ReceiptHandle']
                        )
            except Exception as e:
                print(e)
                continue


if __name__ == '__main__':
    SQSListener().listen()
