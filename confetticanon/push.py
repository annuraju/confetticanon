import boto3
from confetticanon import config
from json import JSONEncoder


def push(message: str):
    sqs = boto3.client('sqs')
    msg_body = JSONEncoder().encode(message)
    sqs.send_message(
        QueueUrl=config.QUEUE_URL,
        MessageBody=msg_body
    )