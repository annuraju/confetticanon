from distutils.core import setup

setup(name='confetticanon',
      version='1.0',
      description='Confetti canon code for Raspberry Pi',
      packages=['confetticanon'],
     )